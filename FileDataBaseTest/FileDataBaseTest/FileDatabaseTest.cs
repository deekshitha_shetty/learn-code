﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileDatabase;
using System.IO;

namespace FileDataBaseTest
{
    [TestClass]
    public class FileDatabaseTest
    {
        string fileData = File.ReadAllText("Student.json");
        Student student = new Student()
        {
            id = 1,
            age = 21,
            name = "Deekshitha shetty"
        };
        ITTDB database = new ITTDB();
        [TestMethod]
        public void Test_SaveData()
        {
            Object fileLengthAfterSave = database.save(student);
            Assert.AreNotEqual(fileData.Length, fileLengthAfterSave);
        }

        [TestMethod]
        public void Test_SaveEmpty()
        {
            Object fileLengthAfterSave = database.save("");
            Assert.AreEqual(fileData.Length, fileLengthAfterSave);
        }

        [TestMethod]
        public void Test_SaveText()
        {
            Object fileLengthAfterSave = database.save("Hi DB");
            Assert.AreEqual(fileData.Length, fileLengthAfterSave);
        }

        [TestMethod]
        public void Test_DeleteData()
        {
            Object fileLengthAfterDelete = database.delete(typeof(Student), 1, "id");
            Assert.AreNotEqual(fileData.Length, fileLengthAfterDelete);
        }

        [TestMethod]
        public void Test_DeleteInvalidData()
        {
            Object fileLengthAfterDelete = database.delete(typeof(Student), "111", "id");
            Assert.AreNotEqual(fileData.Length, fileLengthAfterDelete);
        }

        [TestMethod]
        public void Test_FindData()
        {
            Object data = database.find(typeof(Student), 1, "id");
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void Test_FindInvalidData()
        {
            Object data = database.find(typeof(Student), "1", "id");
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void Test_ModifyData()
        {
            bool isSuccess = database.modify(typeof(Student), 1, 102, "id");
            Assert.IsTrue(isSuccess);
        }

        [TestMethod]
        public void Test_ModifyInvalidData()
        {
            bool isSuccess = database.modify(typeof(Student), "1", 102, "id");
            Assert.IsTrue(isSuccess);
        }

    }
}
