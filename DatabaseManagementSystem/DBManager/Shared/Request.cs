﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManager.Shared
{
    public class Request
    {
        public string Type;
        public Student NewData;
        public string DatabaseName;
        public dynamic FindData;
        public dynamic DeleteData;
        public string DataType;
        public dynamic CurrentValue;
        public dynamic ModifyValue;
    }
}
