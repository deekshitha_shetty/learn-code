﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManager.Shared
{
    public class Response
    {
        public string Status;
        public string Error_Message;
        public dynamic Data;
    }
}
