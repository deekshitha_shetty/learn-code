﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace ITTDBServer
{
        public class ITTDB
        {
            FileManager file = new FileManager();
            ObjectManager serializationObject = new ObjectManager();
           
            public string save(string dataType, dynamic data)
            {
                string fileName = file.GetfileName(dataType);
                data.Id = file.AutoSetId(fileName);
                serializationObject.doSerialization(fileName, data);
                file.FileModifications(fileName);
                return serializationObject.serializeObject(data);
            }

            public dynamic find(string dataType, dynamic data, string fieldType)
            {
                string fileName = file.GetfileName(dataType);
                JArray fileData = serializationObject.doDeserialization(fileName);
                JObject dataForId = fileData.Children<JObject>().FirstOrDefault(o => o[fieldType].ToString() == data.ToString());
                string filedata = dataForId.ToString();
                if (filedata != null)
                    return serializationObject.serializeObject(dataForId);
                else
                    throw new Exception("No Such data available");
            }

            public dynamic modify(string dataType, dynamic actualValue, dynamic modifyValue, string fieldType)
            {
                string fileName = file.GetfileName(dataType);
                string fileData = File.ReadAllText(fileName);
                JArray fileContent = serializationObject.doDeserialization(fileName);
                JObject dataForField = fileContent.Children<JObject>().FirstOrDefault(o => o[fieldType].ToString() == actualValue.ToString());
                dynamic modifiedData = file.ModifyFile(fileData, actualValue, modifyValue, dataForField, fileName);
                return serializationObject.serializeObject(modifiedData);
            }

            public dynamic delete(string dataType, dynamic data, string fieldType)
            {
                string fileName = file.GetfileName(dataType);
                JArray fileData = serializationObject.doDeserialization(fileName);
                JObject dataForId = fileData.Children<JObject>().FirstOrDefault(o => o[fieldType].ToString() == data.ToString());
                string filedata = dataForId.ToString();
                file.CheckForValidData(fileName, filedata);
                return serializationObject.serializeObject(dataForId);
            }
        }
    }
