﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DatabaseManager.Shared;

namespace ITTDBServer
{
    public class FileManager
    {
        ObjectManager serializationObject = new ObjectManager();

        public string GetfileName(string fileName)
        {
            return fileName + ".json";
        }

        public int AutoSetId(string fileName)
        {
            int numberOfObjects = 0;
            if (File.Exists(fileName))
            {
                dynamic fileData = File.ReadAllText(fileName);
                Student[] datajson = JsonConvert.DeserializeObject<Student[]>(fileData);
                numberOfObjects = datajson == null ? 0 : datajson.Count();
            }
            return ++numberOfObjects;
        }
      
        public void FileModifications(string fileName)
        {
            string fileData = File.ReadAllText(fileName);
            fileData = fileData.Replace("[", "").Replace("]", "");
            fileData = fileData.Replace("}{", "},{");
            fileData = "[" + fileData + "]";
            File.WriteAllText(fileName, fileData);
        }

        public dynamic ModifyFile(string fileData, dynamic actualValue, dynamic modifyValue, JObject dataForField, string fileName)
        {
            if (dataForField != null)
            {
                fileData = fileData.Replace(actualValue.ToString(), modifyValue.ToString());
                serializationObject.doSerialization(fileName, fileData);
                FileModifications(fileName);
                return fileData;
            }
            else
            {
                throw new Exception("there is no matching data found");
            }
        }

        public void CheckForValidData(string fileName, string fileData)
        {
            if (fileData != null)
            {
                string fileContent = File.ReadAllText(fileName);
                fileContent = fileContent.Replace(fileData, "");
                serializationObject.doSerialization(fileName, fileContent);
                FileModifications(fileName);
            }
            else
            {
                throw new Exception("No data found");
            }

        }
    }
}
