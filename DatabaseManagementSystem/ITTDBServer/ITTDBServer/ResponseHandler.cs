﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBServer
{
    public class ResponseHandler
    {
        public dynamic getResponse(dynamic requestFromClient)
        {
            dynamic response = null;
            ITTDB dataBase = new ITTDB();
            if (requestFromClient.Type == "Find")
                response = dataBase.find(requestFromClient.DatabaseName, requestFromClient.FindData, requestFromClient.DataType);
            else if (requestFromClient.Type == "Modify")
                response = dataBase.modify(requestFromClient.DatabaseName, requestFromClient.CurrentValue, requestFromClient.ModifyValue, requestFromClient.DataType);
            else if (requestFromClient.Type == "Save")
                response = dataBase.save(requestFromClient.DatabaseName, requestFromClient.NewData);
            else if (requestFromClient.Type == "Delete")
                response = dataBase.delete(requestFromClient.DatabaseName, requestFromClient.DeleteData, requestFromClient.DataType);
            return response;
        }
    }
}
