﻿using DatabaseManager.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ITTDBServer
{
    public class ServerManager
    {
       
        public void ExecuteServer()
        {
            ServerConnectionHandler serverConnectionHandler = new ServerConnectionHandler();
            ResponseHandler responseHandler = new ResponseHandler();

            IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);
            Socket clientSocket = serverConnectionHandler.SetUpServer(ipAddr, localEndPoint);
            try
            {
                serverConnectionHandler.StopServer(clientSocket);
            }
            catch (Exception e)
            {
                Socket response = serverConnectionHandler.CreateResponse("Failure", e.Message.ToString(), null);
                serverConnectionHandler.SendResponse(clientSocket, response);
            }
            Console.ReadKey();
        }
    }
}
