﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DatabaseManager.Shared;

namespace ITTDBServer
{
    public class ObjectManager
    {
        public void doSerialization(string fileName, object data)
        {
            using (TextWriter text = new StreamWriter(fileName, true))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(text, data);
            }
        }

        public JArray doDeserialization(string fileName)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(File.ReadAllText(fileName));
        }

        public dynamic serializeObject(object data)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public dynamic deSerializeObject(dynamic data)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Student>(data);
        }
    }
}
