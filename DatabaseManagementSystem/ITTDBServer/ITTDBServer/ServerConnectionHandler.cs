﻿using DatabaseManager.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ITTDBServer
{
    public class ServerConnectionHandler
    {
        ResponseHandler responseHandler = new ResponseHandler();
        ObjectManager serializationObject = new ObjectManager();

        public Socket SetUpServer(IPAddress ipAddr, IPEndPoint localEndPoint)
        {
            Socket listner = StartServer(ipAddr);
            Bind(listner, localEndPoint);
            Process(listner);
            return Accept(listner);
        }
       
        private Socket StartServer(IPAddress ipAddr)
        {
            return new Socket(ipAddr.AddressFamily,
                         SocketType.Stream, ProtocolType.Tcp);
        }

        private void Bind(Socket listener, IPEndPoint localEndPoint)
        {
            listener.Bind(localEndPoint);
        }

        private void Process(Socket listener)
        {
            listener.Listen(10);
        }

        private Socket Accept(Socket listener)
        {
            Socket clientSocket;
            Console.WriteLine("Waiting connection ... ");

            while (true)
            {
                clientSocket  = listener.Accept();
                Thread thread = new Thread(() => HandleRequestandResponse(clientSocket));
                thread.Start();
            }
            return clientSocket;
        }

        private dynamic ReceiveRequest(Socket clientSocket)
        {
            // Data buffer 
            byte[] bytes = new Byte[1024];
            string data = null;

            while (true)
            {

                int numByte = clientSocket.Receive(bytes);

                data += Encoding.ASCII.GetString(bytes,
                                           0, numByte);

                if (data != null)
                    break;
            }
            Console.WriteLine("Text received -> {0} ", data);
            return data;
        }

        public void SendResponse(Socket clientSocket, dynamic responseFromServer)
        {
            dynamic response = Newtonsoft.Json.JsonConvert.SerializeObject(responseFromServer, Formatting.Indented);
            byte[] message = Encoding.ASCII.GetBytes(response);

            clientSocket.Send(message);
        }

        public dynamic CreateResponse(string status,string errorMessage, dynamic responseFromServer)
        {
            Response response = new Response();
            response.Status = status;
            response.Error_Message = errorMessage;
            response.Data = responseFromServer;
            return response;
        }
        public void StopServer(Socket clientSocket)
        {
            clientSocket.Shutdown(SocketShutdown.Both);
            clientSocket.Close();
        }

        public void HandleRequestandResponse(Socket clientSocket)
        {
            dynamic requestFromClient =  ReceiveRequest(clientSocket);
            var request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(requestFromClient);
            Student response = serializationObject.deSerializeObject(responseHandler.getResponse(request));
            dynamic responseFromServer = CreateResponse("Success", "None", response);
            SendResponse(clientSocket, responseFromServer);
        }

    }
}
