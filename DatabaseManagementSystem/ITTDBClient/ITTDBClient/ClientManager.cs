﻿using ITTDBServer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBClient
{
    public class ClientManager
    {
        public void ExecuteClient()
        {

            try
            {
                RequestHandler requestHandler = new RequestHandler();
                ClientConnectionHandler clientConnectionHandler = new ClientConnectionHandler();
                ObjectManager serializationObject = new ObjectManager();
 
                IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddr = ipHost.AddressList[0];
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);

                Socket sender = clientConnectionHandler.StartClient(ipAddr);
                try
                {
                    clientConnectionHandler.Connect(sender, localEndPoint);
                    dynamic request = serializationObject.serializeObject(requestHandler.GetRequest());
                    clientConnectionHandler.SendRequest(sender, request);
                    clientConnectionHandler.ReceiveResponse(sender);
                    clientConnectionHandler.StopServer(sender);
                }
 
                catch (ArgumentNullException ane)
                {

                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }

                catch (SocketException se)
                {

                    Console.WriteLine("SocketException : {0}", se.ToString());
                }

                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }
            }

            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            Console.ReadKey();
        }
    }
}
