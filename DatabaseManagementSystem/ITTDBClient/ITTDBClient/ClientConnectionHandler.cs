﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBClient
{
    public class ClientConnectionHandler
    {
        public Socket StartClient(IPAddress ipAddr)
        {
            return new Socket(ipAddr.AddressFamily,
                           SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect(Socket sender, IPEndPoint localEndPoint)
        {
            sender.Connect(localEndPoint);
            Console.WriteLine("Socket connected to -> {0} ",
                                 sender.RemoteEndPoint.ToString());
        }

        public void SendRequest(Socket sender, dynamic request)
        { 
            byte[] messageSent = Encoding.ASCII.GetBytes(request);
            int byteSent = sender.Send(messageSent);
        }

        public void ReceiveResponse(Socket sender)
        {
            byte[] messageReceived = new byte[1024];
            int byteRecv = sender.Receive(messageReceived);
            Console.WriteLine("Message from Server -> {0}",
                  Encoding.ASCII.GetString(messageReceived,
                                             0, byteRecv));
        }

        public void StopServer(Socket sender)
        {
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }
}
