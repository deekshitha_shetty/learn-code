﻿using DatabaseManager.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBClient
{
    public class RequestHandler
    {
        public dynamic GetRequest()
        {
            dynamic requestedData = null;
            Console.WriteLine("Enter the operation to proceed");
            Console.WriteLine(" 1:Find \n 2:Save \n 3:Modify \n 4:Delete \n 5:Exit");
            int operation = Convert.ToInt32(Console.ReadLine());
            switch (operation)
            {
                case 1:
                    requestedData = GetFindRequestData();
                    break;
                case 2:
                    requestedData = GetSaveRequestData();
                    break;
                case 3:
                    requestedData = GetModifyRequestData();
                    break;
                case 4:
                    requestedData = GetDeleteRequestData();
                    break;
                case 5:
                    Environment.Exit(0);
                    break;
                default: return "default";
            }
            return requestedData;

        }

        private dynamic GetFindRequestData()
        {
            Request requestData = new Request();
            requestData.Type = "Find";
            requestData.DatabaseName = "Student";
            requestData.DataType = "Id";
            requestData.FindData = 1;
            return requestData;
        }

        private dynamic GetSaveRequestData()
        {
            Student newData = new Student();
            Random random = new Random();
            newData.Age = random.Next();
            newData.Name = "hi";
            Request requestData = new Request();
            requestData.Type = "Save";
            requestData.DatabaseName = "Student";
            requestData.NewData = newData;
            return requestData;
        }

        private dynamic GetModifyRequestData()
        {
            Request requestData = new Request();
            requestData.Type = "Modify";
            requestData.DatabaseName = "Student";
            requestData.DataType = "Name";
            requestData.CurrentValue = "hi";
            requestData.ModifyValue = "bi";
            return requestData;
        }
        private dynamic GetDeleteRequestData()
        {
            Request requestData = new Request();
            requestData.Type = "Delete";
            requestData.DatabaseName = "Student";
            requestData.DataType = "Name";
            requestData.DeleteData = "hi";
            return requestData;
        }
    }
}
