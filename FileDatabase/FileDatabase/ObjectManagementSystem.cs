﻿using System;
using System.Collections.Generic;
using System.IO;
using FileDatabase;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FileDatabase
{
    class ObjectManagementSystem
    {
        /// <summary>
        /// Method to do the serialization
        /// </summary>
        public void doSerialization(string fileName, object data)
        {
            using (TextWriter text = new StreamWriter(fileName, true))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(text, data);
            }
        }

        /// <summary>
        /// Method to do the deserialization
        /// </summary>
        public JArray doDeserialization(string fileName)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(File.ReadAllText(fileName));
        }
    }
}
