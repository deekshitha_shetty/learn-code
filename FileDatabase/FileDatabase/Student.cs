using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDatabase
{
    public class Student
    {

        public int id { set; get; }
        public String name { set; get; }
        public int age { set; get; }

        static void Main(string[] args)
        {
            Student student = new Student()
            {
                id = 1,
                age = 21,
                name = "Deekshitha shetty",
            };
            ITTDB database = new ITTDB();
            database.save(student);
            var students = database.find(typeof(Student), 1, "id");
            database.modify(typeof(Student), 1, 102, "id");
            database.delete(typeof(Student), 1, "id");
        }
    }
}
