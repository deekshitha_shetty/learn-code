﻿using System;
using System.Collections.Generic;
using System.IO;
using FileDatabase;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FileDatabase
{
    class FileManagementSystem
    {
        ObjectManagementSystem serializationObject = new ObjectManagementSystem();

        /// <summary>
        /// Method to get the filename
        /// </summary>
        public string getfileName(dynamic data)
        {
            string fileName = data.GetType().Name;
            if (fileName == "List")
            {
                foreach (var value in data)
                {
                    return value.GetType().Name + ".json";
                }
            }
            return fileName + ".json";
        }

        /// <summary>
        /// method to do the file modifications
        /// </summary>
        public void fileModifications(string fileName)
        {
            string fileData = File.ReadAllText(fileName);
            fileData = fileData.Replace("[", "").Replace("]", "");
            fileData = fileData.Replace("}{", "},{");
            fileData = "[" + fileData + "]";
            File.WriteAllText(fileName, fileData);
        }

        /// <summary>
        /// Method to do the modification
        /// </summary>
        public void ModifyFile(string fileData, dynamic actualValue, dynamic modifyValue, JObject dataForField, string fileName)
        {
            if (dataForField != null)
            {
                fileData = fileData.Replace(actualValue.ToString(), modifyValue.ToString());
                serializationObject.doSerialization(fileName, fileData);
                fileModifications(fileName);
            }
            else
            {
                throw new Exception("there is no matching data found");
            }
        }

        /// <summary>
        /// Method to check valid data
        /// </summary>
        public void checkForValidData(string fileName, string fileData)
        {
            if (fileData != null)
            {
                string fileContent = File.ReadAllText(fileName);
                fileContent = fileContent.Replace(fileData, "");
                serializationObject.doSerialization(fileName, fileContent);
                fileModifications(fileName);
            }
            else
            {
                throw new Exception("No data found");
            }

        }
    }
}
